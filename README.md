# UNICEF

## Instalación

### Registrar recursos para desarrollo

```
$PAYARA_HOME/bin/asadmin add-resources ./setup/payara-resources-devel.xml
```

## Consulta de API de servicios
### Activar el OpenAPI

```
$PAYARA_HOME/bin/asadmin set-openapi-configuration --enabled=true
```

### Acceder al OpenAPI
Entrar a (http://localhost:8080/openapi)