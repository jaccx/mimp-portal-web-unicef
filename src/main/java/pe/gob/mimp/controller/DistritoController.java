package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Distrito;
import pe.gob.mimp.service.DistritoService;

@Path("distrito")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class DistritoController {

    @Inject
    private DistritoService service;

    @POST
    @Path("/addDistrito")
    public Distrito addDistrito(Distrito distrito) {
        return service.saveDistrito(distrito);
    }

    @POST
    @Path("/addDistritos")
    public List<Distrito> addDistritos(List<Distrito> distritos) {
        return service.saveDistritos(distritos);
    }

    @GET
    @Path("/distritos")
    public List<Distrito> findAllDistritos() {
        return service.getDistritos();
    }

    @GET
    @Path("/distritoById/{id}")
    public Distrito findDistritoById(@PathParam("id") int id) {
        return service.getDistritoById(id);
    }

    @GET
    @Path("/distrito/{name}")
    public Distrito findDistritoByName(@PathParam("name") String name) {
        return service.getDistritoByName(name);
    }

    @PUT
    @Path("/distrito/update")
    public Distrito updateDistrito(Distrito distrito) {
        return service.updateDistrito(distrito);
    }

    @DELETE
    @Path("/distrito/delete/{id}")
    public String deleteDistrito(@PathParam("id") int id) {
        return service.deleteDistrito(id);
    }
}
