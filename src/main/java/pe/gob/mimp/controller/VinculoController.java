package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Vinculo;
import pe.gob.mimp.service.VinculoService;

@Path("vinculo")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class VinculoController {

    @Inject
    private VinculoService service;

    @POST
    @Path("/addVinculo")
    public Vinculo addVinculo(Vinculo vinculo) {
        return service.saveVinculo(vinculo);
    }

    @POST
    @Path("/addVinculos")
    public List<Vinculo> addVinculos(List<Vinculo> vinculos) {
        return service.saveVinculos(vinculos);
    }

    @GET
    @Path("/vinculos")
    public List<Vinculo> findAllVinculos() {
        return service.getVinculos();
    }

    @GET
    @Path("/vinculoById/{id}")
    public Vinculo findVinculoById(@PathParam("id") int id) {
        return service.getVinculoById(id);
    }

    @GET
    @Path("/vinculo/{name}")
    public Vinculo findVinculoByName(@PathParam("name") String name) {
        return service.getVinculoByName(name);
    }

    @PUT
    @Path("/vinculo/update")
    public Vinculo updateVinculo(Vinculo vinculo) {
        return service.updateVinculo(vinculo);
    }

    @DELETE
    @Path("/vinculo/delete/{id}")
    public String deleteVinculo(@PathParam("id") int id) {
        return service.deleteVinculo(id);
    }
}
