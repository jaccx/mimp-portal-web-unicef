package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Cuenta;
import pe.gob.mimp.service.CuentaService;

@Path("cuenta")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class CuentaController {

    @Inject
    private CuentaService service;

    @POST
    @Path("/addCuenta")
    public Cuenta addCuenta(Cuenta cuenta) {
        return service.saveCuenta(cuenta);
    }

    @POST
    @Path("/addCuentas")
    public List<Cuenta> addCuentas(List<Cuenta> cuentas) {
        return service.saveCuentas(cuentas);
    }

    @GET
    @Path("/cuentas")
    public List<Cuenta> findAllCuentas() {
        return service.getCuentas();
    }

    @GET
    @Path("/cuentaById/{id}")
    public Cuenta findCuentaById(@PathParam("id") int id) {
        return service.getCuentaById(id);
    }

    @GET
    @Path("/cuenta/{name}")
    public Cuenta findCuentaByName(@PathParam("name") String name) {
        return service.getCuentaByName(name);
    }

    @PUT
    @Path("/cuenta/update")
    public Cuenta updateCuenta(Cuenta cuenta) {
        return service.updateCuenta(cuenta);
    }

    @DELETE
    @Path("/cuenta/delete/{id}")
    public String deleteCuenta(@PathParam("id") int id) {
        return service.deleteCuenta(id);
    }
}
