package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Tipologia;
import pe.gob.mimp.service.TipologiaService;

@Path("tipologia")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class TipologiaController {

    @Inject
    private TipologiaService service;

    @POST
    @Path("/addTipologia")
    public Tipologia addTipologia(Tipologia tipologia) {
        return service.saveTipologia(tipologia);
    }

    @POST
    @Path("/addTipologias")
    public List<Tipologia> addTipologias(List<Tipologia> tipologias) {
        return service.saveTipologias(tipologias);
    }

    @GET
    @Path("/tipologias")
    public List<Tipologia> findAllTipologias() {
        return service.getTipologias();
    }

    @GET
    @Path("/tipologiaById/{id}")
    public Tipologia findTipologiaById(@PathParam("id") int id) {
        return service.getTipologiaById(id);
    }

    @GET
    @Path("/tipologia/{name}")
    public Tipologia findTipologiaByName(@PathParam("name") String name) {
        return service.getTipologiaByName(name);
    }

    @PUT
    @Path("/tipologia/update")
    public Tipologia updateTipologia(Tipologia tipologia) {
        return service.updateTipologia(tipologia);
    }

    @DELETE
    @Path("/tipologia/delete/{id}")
    public String deleteTipologia(@PathParam("id") int id) {
        return service.deleteTipologia(id);
    }
}
