package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Departamento;
import pe.gob.mimp.service.DepartamentoService;

@Path("departamento")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class DepartamentoController {

    @Inject
    private DepartamentoService service;

    @POST
    @Path("/addDepartamento")
    public Departamento addDepartamento(Departamento departamento) {
        return service.saveDepartamento(departamento);
    }

    @POST
    @Path("/addDepartamentos")
    public List<Departamento> addDepartamentos(List<Departamento> departamentos) {
        return service.saveDepartamentos(departamentos);
    }

    @GET
    @Path("/departamentos")
    public List<Departamento> findAllDepartamentos() {
        return service.getDepartamentos();
    }

    @GET
    @Path("/departamentoById/{id}")
    public Departamento findDepartamentoById(@PathParam("id") int id) {
        return service.getDepartamentoById(id);
    }

    @GET
    @Path("/departamento/{name}")
    public Departamento findDepartamentoByName(@PathParam("name") String name) {
        return service.getDepartamentoByName(name);
    }

    @PUT
    @Path("/departamento/update")
    public Departamento updateDepartamento(Departamento departamento) {
        return service.updateDepartamento(departamento);
    }

    @DELETE
    @Path("/departamento/delete/{id}")
    public String deleteDepartamento(@PathParam("id") int id) {
        return service.deleteDepartamento(id);
    }
}
