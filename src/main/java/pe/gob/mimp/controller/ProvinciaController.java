package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Provincia;
import pe.gob.mimp.service.ProvinciaService;

@Path("provincia")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class ProvinciaController {

    @Inject
    private ProvinciaService service;

    @POST
    @Path("/addProvincia")
    public Provincia addProvincia(Provincia provincia) {
        return service.saveProvincia(provincia);
    }

    @POST
    @Path("/addProvincias")
    public List<Provincia> addProvincias(List<Provincia> provincias) {
        return service.saveProvincias(provincias);
    }

    @GET
    @Path("/provincias")
    public List<Provincia> findAllProvincias() {
        return service.getProvincias();
    }

    @GET
    @Path("/provinciaById/{id}")
    public Provincia findProvinciaById(@PathParam("id") int id) {
        return service.getProvinciaById(id);
    }

    @GET
    @Path("/provincia/{name}")
    public Provincia findProvinciaByName(@PathParam("name") String name) {
        return service.getProvinciaByName(name);
    }

    @PUT
    @Path("/provincia/update")
    public Provincia updateProvincia(Provincia provincia) {
        return service.updateProvincia(provincia);
    }

    @DELETE
    @Path("/provincia/delete/{id}")
    public String deleteProvincia(@PathParam("id") int id) {
        return service.deleteProvincia(id);
    }
}
