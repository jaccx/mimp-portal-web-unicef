package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.RangoEdad;
import pe.gob.mimp.service.RangoEdadService;

@Path("rangoedad")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class RangoEdadController {

    @Inject
    private RangoEdadService service;

    @POST
    @Path("/addRangoEdad")
    public RangoEdad addRangoEdad(RangoEdad rangoedad) {
        return service.saveRangoEdad(rangoedad);
    }

    @POST
    @Path("/addRangoEdades")
    public List<RangoEdad> addRangoEdades(List<RangoEdad> rangoedades) {
        return service.saveRangoEdades(rangoedades);
    }

    @GET
    @Path("/rangoedades")
    public List<RangoEdad> findAllRangoEdades() {
        return service.getRangoEdades();
    }

    @GET
    @Path("/rangoedadById/{id}")
    public RangoEdad findRangoEdadById(@PathParam("id") int id) {
        return service.getRangoEdadById(id);
    }

    @GET
    @Path("/rangoedad/{name}")
    public RangoEdad findRangoEdadByName(@PathParam("name") String name) {
        return service.getRangoEdadByName(name);
    }

    @PUT
    @Path("/rangoedad/update")
    public RangoEdad updateRangoEdad(RangoEdad rangoedad) {
        return service.updateRangoEdad(rangoedad);
    }

    @DELETE
    @Path("/rangoedad/delete/{id}")
    public String deleteRangoEdad(@PathParam("id") int id) {
        return service.deleteRangoEdad(id);
    }
}
