package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Perfil;
import pe.gob.mimp.service.PerfilService;

@Path("perfil")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class PerfilController {

    @Inject
    private PerfilService service;

    @POST
    @Path("/addPerfil")
    public Perfil addPerfil(Perfil perfil) {
        return service.savePerfil(perfil);
    }

    @POST
    @Path("/addPerfiles")
    public List<Perfil> addPerfiles(List<Perfil> perfiles) {
        return service.savePerfiles(perfiles);
    }

    @GET
    @Path("/perfiles")
    public List<Perfil> findAllPerfiles() {
        return service.getPerfiles();
    }

    @GET
    @Path("/perfilById/{id}")
    public Perfil findPerfilById(@PathParam("id") int id) {
        return service.getPerfilById(id);
    }

    @GET
    @Path("/perfil/{name}")
    public Perfil findPerfilByName(@PathParam("name") String name) {
        return service.getPerfilByName(name);
    }

    @PUT
    @Path("/perfil/update")
    public Perfil updatePerfil(Perfil perfil) {
        return service.updatePerfil(perfil);
    }

    @DELETE
    @Path("/perfil/delete/{id}")
    public String deletePerfil(@PathParam("id") int id) {
        return service.deletePerfil(id);
    }
}
