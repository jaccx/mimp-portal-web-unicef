package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.EquipoExterno;
import pe.gob.mimp.service.EquipoExternoService;

@Path("equipo")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class EquipoExternoController {

    @Inject
    private EquipoExternoService service;

    @POST
    @Path("/addEquipoExterno")
    public EquipoExterno addEquipoExterno(EquipoExterno equipoexterno) {
        return service.saveEquipoExterno(equipoexterno);
    }

    @POST
    @Path("/addEquipoExternos")
    public List<EquipoExterno> addEquipoExternos(List<EquipoExterno> equipoexternos) {
        return service.saveEquipoExternos(equipoexternos);
    }

    @GET
    @Path("/equipoexternos")
    public List<EquipoExterno> findAllEquipoExternos() {
        return service.getEquipoExternos();
    }

    @GET
    @Path("/equipoexternoById/{id}")
    public EquipoExterno findEquipoExternoById(@PathParam("id") int id) {
        return service.getEquipoExternoById(id);
    }

    @GET
    @Path("/equipoexterno/{name}")
    public EquipoExterno findEquipoExternoByName(@PathParam("name") String name) {
        return service.getEquipoExternoByName(name);
    }

    @PUT
    @Path("/equipoexterno/update")
    public EquipoExterno updateEquipoExterno(EquipoExterno equipoexterno) {
        return service.updateEquipoExterno(equipoexterno);
    }

    @DELETE
    @Path("/equipoexterno/delete/{id}")
    public String deleteEquipoExterno(@PathParam("id") int id) {
        return service.deleteEquipoExterno(id);
    }
}
