package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Motivo;
import pe.gob.mimp.service.MotivoService;

@Path("motivo")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class MotivoController {

    @Inject
    private MotivoService service;

    @POST
    @Path("/addMotivo")
    public Motivo addMotivo(Motivo motivo) {
        return service.saveMotivo(motivo);
    }

    @POST
    @Path("/addMotivos")
    public List<Motivo> addMotivos(List<Motivo> motivos) {
        return service.saveMotivos(motivos);
    }

    @GET
    @Path("/motivos")
    public List<Motivo> findAllMotivos() {
        return service.getMotivos();
    }

    @GET
    @Path("/motivoById/{id}")
    public Motivo findMotivoById(@PathParam("id") int id) {
        return service.getMotivoById(id);
    }

    @GET
    @Path("/motivo/{name}")
    public Motivo findMotivoByName(@PathParam("name") String name) {
        return service.getMotivoByName(name);
    }

    @PUT
    @Path("/motivo/update")
    public Motivo updateMotivo(Motivo motivo) {
        return service.updateMotivo(motivo);
    }

    @DELETE
    @Path("/motivo/delete/{id}")
    public String deleteMotivo(@PathParam("id") int id) {
        return service.deleteMotivo(id);
    }
}
