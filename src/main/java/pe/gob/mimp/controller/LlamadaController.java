package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Llamada;
import pe.gob.mimp.service.LlamadaService;

@Path("llamada")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class LlamadaController {

    @Inject
    private LlamadaService service;

    @POST @Path("/addLlamada")
    public Llamada addLlamada(Llamada llamada) {
        return service.saveLlamada(llamada);
    }

    @POST @Path("/addLlamadas")
    public List<Llamada> addLlamadas(List<Llamada> llamadas) {
        return service.saveLlamadas(llamadas);
    }

    @GET @Path("/llamadas")
    public List<Llamada> findAllLlamadas() {
        return service.getLlamadas();
    }

    @GET @Path("/llamadaById/{id}")
    public Llamada findLlamadaById(@PathParam("id") int id) {
        return service.getLlamadaById(id);
    }

    @PUT @Path("/llamada/update")
    public Llamada updateLlamada(Llamada llamada) {
        return service.updateLlamada(llamada);
    }

    @DELETE @Path("/llamada/delete/{id}")
    public String deleteLlamada(@PathParam("id") int id) {
        return service.deleteLlamada(id);
    }
}
