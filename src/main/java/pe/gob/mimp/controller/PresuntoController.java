package pe.gob.mimp.controller;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import pe.gob.mimp.entity.Presunto;
import pe.gob.mimp.service.PresuntoService;

@Path("presunto")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class PresuntoController {

    @Inject
    private PresuntoService service;

    @POST @Path("/addPresunto")
    public Presunto addPresunto( Presunto presunto) {
        return service.savePresunto(presunto);
    }

    @POST @Path("/addPresuntos")
    public List<Presunto> addPresuntos( List<Presunto> presuntos) {
        return service.savePresuntos(presuntos);
    }

    @GET @Path("/presuntos")
    public List<Presunto> findAllPresuntos() {
        return service.getPresuntos();
    }

    @GET @Path("/presuntoById/{id}")
    public Presunto findPresuntoById(@PathParam("id") int id) {
        return service.getPresuntoById(id);
    }

    @GET @Path("/presunto/{name}")
    public Presunto findPresuntoByName(@PathParam("name") String name) {
        return service.getPresuntoByName(name);
    }

    @PUT @Path("/presunto/update")
    public Presunto updatePresunto( Presunto presunto) {
        return service.updatePresunto(presunto);
    }

    @DELETE @Path("/presunto/delete/{id}")
    public String deletePresunto(@PathParam("id") int id) {
        return service.deletePresunto(id);
    }
}
