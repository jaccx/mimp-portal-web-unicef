package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_PROVINCIA")
public class Provincia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "PROVINCIA_ID")
    private int id;

    @Column(name = "PROVINCIA_NOMBRE")
    private String name;

    @ManyToOne(optional = true)
    @JoinColumn(name="PROVINCIA_DEPARTAMENTO_ID")
    private Departamento departamento;
    
}