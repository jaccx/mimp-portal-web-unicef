package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_FICHA")
public class Ficha implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "FICHA_ID")
    private int id;

    @Column(name = "FICHA_FECHA")
    private Date fecha;

    @OneToOne(optional = true)
    @JoinColumn(name="FICHA_USUARIO_ID", unique = true)
    private Usuario usuario;

    @OneToOne(optional = true)
    @JoinColumn(name="FICHA_LLAMADA_ID", unique = true)
    private Llamada llamada;

    @ManyToOne(optional = true)
    @JoinColumn(name="FICHA_MOTIVO_ID")
    private Motivo motivo;

    @ManyToOne(optional = true)
    @JoinColumn(name="FICHA_CUENTA_ID")
    private Cuenta cuenta;

}