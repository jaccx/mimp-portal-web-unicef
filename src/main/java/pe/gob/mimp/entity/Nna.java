package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_NNA")
public class Nna implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "NNA_ID")
    private int id;

    @Column(name = "NNA_CELULAR")
    private String phone;

    @Column(name = "NNA_APELLIDO_PATERNO")
    private String lastname;

    @Column(name = "NNA_APELLIDO_MATERNO")
    private String lastname2;

    @Column(name = "NNA_NOMBRES")
    private String nombres;

    @Column(name = "NNA_DNI")
    private String dni;

    @Column(name = "NNA_FECHA_NACIMIENTO")
    private Date born;

    @Column(name = "NNA_GENERO")
    private String gender;

    @Column(name = "NNA_DISCAPACIDAD")
    private String discapacidad;

    @Column(name = "NNA_ESTUDIA")
    private String study;

    @Column(name = "NNA_INSTITUCION")
    private String institution;

    @Column(name = "NNA_GESTANTE")
    private String pregnant;

    @Column(name = "NNA_ADOPCION")
    private String adoption;

    @Column(name = "NNA_PADRE_MIGRANTE")
    private String fathermigrant;

    @Column(name = "NNA_MADRE_MIGRANTE")
    private String mothermigrant;

    @Column(name = "NNA_DIRECCION")
    private String address;

    @Column(name = "NNA_referencia")
    private String reference;

    @ManyToOne(optional = true)
    @JoinColumn(name="NNA_FICHA_ID")
    private Ficha ficha;

    @ManyToOne(optional = true)
    @JoinColumn(name="NNA_PRESUNTO_ID")
    private Presunto presunto;

    @ManyToOne(optional = true)
    @JoinColumn(name="NNA_DISTRITO_ID")
    private Distrito distrito;

}