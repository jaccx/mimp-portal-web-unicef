package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_LLAMADA")
public class Llamada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "LLAMADA_ID")
    private int id;

    @Column(name = "LLAMADA_PROCEDIMIENTO")
    private String procedimiento;

    @Column(name = "LLAMADA_PROCEDENCIA")
    private String procedencia;

    @Column(name = "LLAMADA_RESULTADO")
    private String resultado;
}