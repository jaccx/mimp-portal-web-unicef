package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_CUENTA")
public class Cuenta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "CUENTA_ID")
    private int id;

    @Column(name = "CUENTA_NOMBRE")
    private String name;

    @Column(name = "CUENTA_CORREO")
    private String email;

    @Column(name = "CUENTA_CLAVE")
    private String password;

    @ManyToOne(optional = true)
    @JoinColumn(name="CUENTA_PERFIL_ID")
    private Perfil profile;
}