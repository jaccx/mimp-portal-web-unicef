package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_EQUIPO_EXTERNO")
public class EquipoExterno implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "EQUIPO_EXTERNO_ID")
    private int id;

    @Column(name = "EQUIPO_EXTERNO_NOMBRE")
    private String name;

    @Column(name = "EQUIPO_EXTERNO_DESCRIPCION")
    private String description;

    @Column(name = "EQUIPO_EXTERNO_TIPO")
    private int type;
    
}