package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_FICHA_EQUIPO")
public class FichaEquipo implements Serializable {

    @Id
    @ManyToOne(optional = true)
    @JoinColumn(name="FICHA_ID")
    private Ficha ficha;

    @Id
    @ManyToOne(optional = true)
    @JoinColumn(name="EQUIPO_ID")
    private EquipoExterno equipoexterno;

    @Column(name = "FICHA_EQUIPO_FECHA")
    private Date date;
    
}