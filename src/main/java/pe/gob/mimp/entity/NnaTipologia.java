package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_NNA_TIPOLOGIA")
public class NnaTipologia implements Serializable {

    @Id
    @ManyToOne(optional = true)
    @JoinColumn(name="NNA_ID")
    private Nna nna;

    @Id
    @ManyToOne(optional = true)
    @JoinColumn(name="TIPOLOGIA_ID")
    private Tipologia tipologia;

    @Column(name = "NNA_TIPOLOGIA_FECHA")
    private Date date;
    
}