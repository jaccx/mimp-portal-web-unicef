package pe.gob.mimp.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MIMP_USUARIO")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "USUARIO_ID")
    private int id;

    @Column(name = "USUARIO_CELULAR")
    private String phone;

    @Column(name = "USUARIO_APELLIDOS")
    private String lastname;

    @Column(name = "USUARIO_NOMBRES")
    private String firstname;

    @Column(name = "USUARIO_DNI")
    private String dni;

    @Column(name = "USUARIO_CORREO")
    private String email;

    @Column(name = "USUARIO_FECHA_NACIMIENTO")
    private Date born;

    @Column(name = "USUARIO_GENERO")
    private String gender;

    @Column(name = "USUARIO_ANONIMO")
    private String anonymous;

    @Column(name = "USUARIO_SEUDONIMO")
    private String nickname;

    @Column(name = "USUARIO_MIGRANTE")
    private String migrant;

    @Column(name = "USUARIO_DIRECCION")
    private String address;

    @ManyToOne(optional = true)
    @JoinColumn(name="USUARIO_VINCULO_ID")
    private Vinculo vinculo;

    @ManyToOne(optional = true)
    @JoinColumn(name="USUARIO_RANGOEDAD_ID")
    private RangoEdad rangoedad;

    @ManyToOne(optional = true)
    @JoinColumn(name="USUARIO_DISTRITO_ID")
    private Distrito distrito;

}