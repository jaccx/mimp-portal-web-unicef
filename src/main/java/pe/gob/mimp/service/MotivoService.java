package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Motivo;
import pe.gob.mimp.repository.MotivoRepository;

@ApplicationScoped
public class MotivoService {

    @Inject
    private MotivoRepository repository;

    public Motivo saveMotivo(Motivo motivo) {
        return repository.save(motivo);
    }

    public List<Motivo> saveMotivos(List<Motivo> motivos) {
        return repository.saveAll(motivos);
    }

    public List<Motivo> getMotivos() {
        return repository.findAll();
    }

    public Motivo getMotivoById(int id) {
        return repository.findById(id);
    }

    public Motivo getMotivoByName(String name) {
        return repository.findByName(name);
    }

    public String deleteMotivo(int id) {
        repository.deleteById(id);
        return "motivo removed !! " + id;
    }

    public Motivo updateMotivo(Motivo motivo) {
        Motivo existingMotivo = repository.findById(motivo.getId());
        existingMotivo.setName(motivo.getName());
        existingMotivo.setDescription(motivo.getDescription());
        return repository.save(existingMotivo);
    }

}
