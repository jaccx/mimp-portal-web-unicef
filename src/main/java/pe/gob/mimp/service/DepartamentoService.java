package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Departamento;
import pe.gob.mimp.repository.DepartamentoRepository;

@ApplicationScoped
public class DepartamentoService {

    @Inject
    private DepartamentoRepository repository;

    public Departamento saveDepartamento(Departamento departamento) {
        return repository.save(departamento);
    }

    public List<Departamento> saveDepartamentos(List<Departamento> departamentos) {
        return repository.saveAll(departamentos);
    }

    public List<Departamento> getDepartamentos() {
        return repository.findAll();
    }

    public Departamento getDepartamentoById(int id) {
        return repository.findById(id);
    }

    public Departamento getDepartamentoByName(String name) {
        return repository.findByName(name);
    }

    public String deleteDepartamento(int id) {
        repository.deleteById(id);
        return "departamento removed !! " + id;
    }

    public Departamento updateDepartamento(Departamento departamento) {
        Departamento existingDepartamento = repository.findById(departamento.getId());
        existingDepartamento.setName(departamento.getName());
        return repository.save(existingDepartamento);
    }

}
