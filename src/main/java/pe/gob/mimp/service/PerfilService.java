package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Perfil;
import pe.gob.mimp.repository.PerfilRepository;

@ApplicationScoped
public class PerfilService {

    @Inject
    private PerfilRepository repository;

    public Perfil savePerfil(Perfil perfil) {
        return repository.save(perfil);
    }

    public List<Perfil> savePerfiles(List<Perfil> perfiles) {
        return repository.saveAll(perfiles);
    }

    public List<Perfil> getPerfiles() {
        return repository.findAll();
    }

    public Perfil getPerfilById(int id) {
        return repository.findById(id);
    }

    public Perfil getPerfilByName(String name) {
        return repository.findByName(name);
    }

    public String deletePerfil(int id) {
        repository.deleteById(id);
        return "perfil removed !! " + id;
    }

    public Perfil updatePerfil(Perfil perfil) {
        Perfil existingPerfil = repository.findById(perfil.getId());
        existingPerfil.setName(perfil.getName());
        existingPerfil.setDescription(perfil.getDescription());
        return repository.save(existingPerfil);
    }

}
