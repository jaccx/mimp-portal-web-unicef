package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Presunto;
import pe.gob.mimp.repository.PresuntoRepository;

@ApplicationScoped
public class PresuntoService {

    @Inject
    private PresuntoRepository repository;

    public Presunto savePresunto(Presunto presunto) {
        return repository.save(presunto);
    }

    public List<Presunto> savePresuntos(List<Presunto> presuntos) {
        return repository.saveAll(presuntos);
    }

    public List<Presunto> getPresuntos() {
        return repository.findAll();
    }

    public Presunto getPresuntoById(int id) {
        return repository.findById(id);
    }

    public Presunto getPresuntoByName(String name) {
        return repository.findByName(name);
    }

    public String deletePresunto(int id) {
        repository.deleteById(id);
        return "presunto removed !! " + id;
    }

    public Presunto updatePresunto(Presunto presunto) {
        Presunto existingPresunto = repository.findById(presunto.getId());
        existingPresunto.setName(presunto.getName());
        existingPresunto.setDescription(presunto.getDescription());
        return repository.save(existingPresunto);
    }

}
