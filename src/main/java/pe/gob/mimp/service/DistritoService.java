package pe.gob.mimp.service;

import pe.gob.mimp.entity.Distrito;
import pe.gob.mimp.repository.DistritoRepository;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DistritoService {

    @Inject
    private DistritoRepository repository;

    public Distrito saveDistrito(Distrito provincia) {
        return repository.save(provincia);
    }

    public List<Distrito> saveDistritos(List<Distrito> provincias) {
        return repository.saveAll(provincias);
    }

    public List<Distrito> getDistritos() {
        return repository.findAll();
    }

    public Distrito getDistritoById(int id) {
        return repository.findById(id);
    }

    public Distrito getDistritoByName(String name) {
        return repository.findByName(name);
    }

    public String deleteDistrito(int id) {
        repository.deleteById(id);
        return "provincia removed !! " + id;
    }

    public Distrito updateDistrito(Distrito provincia) {
        Distrito existingDistrito = repository.findById(provincia.getId());
        existingDistrito.setName(provincia.getName());
        existingDistrito.setProvincia(provincia.getProvincia());
        return repository.save(existingDistrito);
    }

}
