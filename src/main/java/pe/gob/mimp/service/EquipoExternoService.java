package pe.gob.mimp.service;

import pe.gob.mimp.entity.EquipoExterno;
import pe.gob.mimp.repository.EquipoExternoRepository;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class EquipoExternoService {

    @Inject
    private EquipoExternoRepository repository;

    public EquipoExterno saveEquipoExterno(EquipoExterno equipoexterno) {
        return repository.save(equipoexterno);
    }

    public List<EquipoExterno> saveEquipoExternos(List<EquipoExterno> equipoexternos) {
        return repository.saveAll(equipoexternos);
    }

    public List<EquipoExterno> getEquipoExternos() {
        return repository.findAll();
    }

    public EquipoExterno getEquipoExternoById(int id) {
        return repository.findById(id);
    }

    public EquipoExterno getEquipoExternoByName(String name) {
        return repository.findByName(name);
    }

    public String deleteEquipoExterno(int id) {
        repository.deleteById(id);
        return "equipoexterno removed !! " + id;
    }

    public EquipoExterno updateEquipoExterno(EquipoExterno equipoexterno) {
        EquipoExterno existingEquipoExterno = repository.findById(equipoexterno.getId());
        existingEquipoExterno.setName(equipoexterno.getName());
        existingEquipoExterno.setDescription(equipoexterno.getDescription());
        existingEquipoExterno.setType(equipoexterno.getType());
        return repository.save(existingEquipoExterno);
    }

}
