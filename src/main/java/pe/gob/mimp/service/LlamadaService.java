package pe.gob.mimp.service;

import pe.gob.mimp.entity.Llamada;
import pe.gob.mimp.repository.LlamadaRepository;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class LlamadaService {

    @Inject
    private LlamadaRepository repository;

    public Llamada saveLlamada(Llamada llamada) {
        return repository.save(llamada);
    }

    public List<Llamada> saveLlamadas(List<Llamada> llamadas) {
        return repository.saveAll(llamadas);
    }

    public List<Llamada> getLlamadas() {
        return repository.findAll();
    }

    public Llamada getLlamadaById(int id) {
        return repository.findById(id);
    }

    public String deleteLlamada(int id) {
        repository.deleteById(id);
        return "llamada removed !! " + id;
    }

    public Llamada updateLlamada(Llamada llamada) {
        Llamada existingLlamada = repository.findById(llamada.getId());
        existingLlamada.setProcedimiento(llamada.getProcedimiento());
        existingLlamada.setProcedencia(llamada.getProcedencia());
        existingLlamada.setResultado(llamada.getResultado());
        return repository.save(existingLlamada);
    }

}
