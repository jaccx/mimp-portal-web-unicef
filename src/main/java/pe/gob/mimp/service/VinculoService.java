package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Vinculo;
import pe.gob.mimp.repository.VinculoRepository;

@ApplicationScoped
public class VinculoService {

    @Inject
    private VinculoRepository repository;

    public Vinculo saveVinculo(Vinculo vinculo) {
        return repository.save(vinculo);
    }

    public List<Vinculo> saveVinculos(List<Vinculo> vinculos) {
        return repository.saveAll(vinculos);
    }

    public List<Vinculo> getVinculos() {
        return repository.findAll();
    }

    public Vinculo getVinculoById(int id) {
        return repository.findById(id);
    }

    public Vinculo getVinculoByName(String name) {
        return repository.findByName(name);
    }

    public String deleteVinculo(int id) {
        repository.deleteById(id);
        return "vinculo removed !! " + id;
    }

    public Vinculo updateVinculo(Vinculo vinculo) {
        Vinculo existingVinculo = repository.findById(vinculo.getId());
        existingVinculo.setName(vinculo.getName());
        existingVinculo.setDescription(vinculo.getDescription());
        return repository.save(existingVinculo);
    }


}
