package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Tipologia;
import pe.gob.mimp.repository.TipologiaRepository;

@ApplicationScoped
public class TipologiaService {

    @Inject
    private TipologiaRepository repository;

    public Tipologia saveTipologia(Tipologia tipologia) {
        return repository.save(tipologia);
    }

    public List<Tipologia> saveTipologias(List<Tipologia> tipologias) {
        return repository.saveAll(tipologias);
    }

    public List<Tipologia> getTipologias() {
        return repository.findAll();
    }

    public Tipologia getTipologiaById(int id) {
        return repository.findById(id);
    }

    public Tipologia getTipologiaByName(String name) {
        return repository.findByName(name);
    }

    public String deleteTipologia(int id) {
        repository.deleteById(id);
        return "tipologia removed !! " + id;
    }

    public Tipologia updateTipologia(Tipologia tipologia) {
        Tipologia existingTipologia = repository.findById(tipologia.getId());
        existingTipologia.setName(tipologia.getName());
        existingTipologia.setDescription(tipologia.getDescription());
        return repository.save(existingTipologia);
    }


}
