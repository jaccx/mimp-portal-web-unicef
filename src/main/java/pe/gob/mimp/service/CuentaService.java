package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Cuenta;
import pe.gob.mimp.repository.CuentaRepository;

@ApplicationScoped
public class CuentaService {

    @Inject
    private CuentaRepository repository;

    public Cuenta saveCuenta(Cuenta cuenta) {
        return repository.save(cuenta);
    }

    public List<Cuenta> saveCuentas(List<Cuenta> cuentas) {
        return repository.saveAll(cuentas);
    }

    public List<Cuenta> getCuentas() {
        return repository.findAll();
    }

    public Cuenta getCuentaById(int id) {
        return repository.findById(id);
    }

    public Cuenta getCuentaByName(String name) {
        return repository.findByName(name);
    }

    public String deleteCuenta(int id) {
        repository.deleteById(id);
        return "cuenta removed !! " + id;
    }

    public Cuenta updateCuenta(Cuenta cuenta) {
        Cuenta existingCuenta = repository.findById(cuenta.getId());
        existingCuenta.setName(cuenta.getName());
        existingCuenta.setEmail(cuenta.getEmail());
        existingCuenta.setPassword(cuenta.getPassword());
        existingCuenta.setProfile(cuenta.getProfile());
        return repository.save(existingCuenta);
    }

}
