package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.Provincia;
import pe.gob.mimp.repository.ProvinciaRepository;

@ApplicationScoped
public class ProvinciaService {

    @Inject
    private ProvinciaRepository repository;

    public Provincia saveProvincia(Provincia provincia) {
        return repository.save(provincia);
    }

    public List<Provincia> saveProvincias(List<Provincia> provincias) {
        return repository.saveAll(provincias);
    }

    public List<Provincia> getProvincias() {
        return repository.findAll();
    }

    public Provincia getProvinciaById(int id) {
        return repository.findById(id);
    }

    public Provincia getProvinciaByName(String name) {
        return repository.findByName(name);
    }

    public String deleteProvincia(int id) {
        repository.deleteById(id);
        return "provincia removed !! " + id;
    }

    public Provincia updateProvincia(Provincia provincia) {
        Provincia existingProvincia = repository.findById(provincia.getId());
        existingProvincia.setName(provincia.getName());
        existingProvincia.setDepartamento(provincia.getDepartamento());
        return repository.save(existingProvincia);
    }

}
