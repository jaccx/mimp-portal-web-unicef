package pe.gob.mimp.service;

import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import pe.gob.mimp.entity.RangoEdad;
import pe.gob.mimp.repository.RangoEdadRepository;

@ApplicationScoped
public class RangoEdadService {

    @Inject
    private RangoEdadRepository repository;

    public RangoEdad saveRangoEdad(RangoEdad rangoedad) {
        return repository.save(rangoedad);
    }

    public List<RangoEdad> saveRangoEdades(List<RangoEdad> rangoedades) {
        return repository.saveAll(rangoedades);
    }

    public List<RangoEdad> getRangoEdades() {
        return repository.findAll();
    }

    public RangoEdad getRangoEdadById(int id) {
        return repository.findById(id);
    }

    public RangoEdad getRangoEdadByName(String name) {
        return repository.findByName(name);
    }

    public String deleteRangoEdad(int id) {
        repository.deleteById(id);
        return "rangoedad removed !! " + id;
    }

    public RangoEdad updateRangoEdad(RangoEdad rangoedad) {
        RangoEdad existingRangoEdad = repository.findById(rangoedad.getId());
        existingRangoEdad.setName(rangoedad.getName());
        existingRangoEdad.setDescription(rangoedad.getDescription());
        return repository.save(existingRangoEdad);
    }

}
