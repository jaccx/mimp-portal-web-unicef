package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Distrito;

@ApplicationScoped
public class DistritoRepository extends AbstractRepository<Distrito, Integer> {

    @Inject
    private EntityManager em;

    public DistritoRepository() {
        super(Distrito.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
