package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.EquipoExterno;

@ApplicationScoped
public class EquipoExternoRepository extends AbstractRepository<EquipoExterno, Integer> {

    @Inject
    private EntityManager em;

    public EquipoExternoRepository() {
        super(EquipoExterno.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
