package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Motivo;

@ApplicationScoped
public class MotivoRepository extends AbstractRepository<Motivo, Integer> {

    @Inject
    private EntityManager em;

    public MotivoRepository() {
        super(Motivo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
