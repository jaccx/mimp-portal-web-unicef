package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Provincia;

@ApplicationScoped
public class ProvinciaRepository extends AbstractRepository<Provincia, Integer> {

    @Inject
    private EntityManager em;

    public ProvinciaRepository() {
        super(Provincia.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
