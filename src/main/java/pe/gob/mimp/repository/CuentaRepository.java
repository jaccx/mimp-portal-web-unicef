package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Cuenta;

@ApplicationScoped
public class CuentaRepository extends AbstractRepository<Cuenta, Integer> {

    @Inject
    private EntityManager em;

    public CuentaRepository() {
        super(Cuenta.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
