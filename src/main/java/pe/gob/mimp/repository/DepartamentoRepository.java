package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Departamento;

@ApplicationScoped
public class DepartamentoRepository extends AbstractRepository<Departamento, Integer> {

    @Inject
    private EntityManager em;

    public DepartamentoRepository() {
        super(Departamento.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
