package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Vinculo;

@ApplicationScoped
public class VinculoRepository extends AbstractRepository<Vinculo, Integer> {

    @Inject
    private EntityManager em;

    public VinculoRepository() {
        super(Vinculo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
