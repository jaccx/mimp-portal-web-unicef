package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Perfil;

@ApplicationScoped
public class PerfilRepository extends AbstractRepository<Perfil, Integer> {

    @Inject
    private EntityManager em;

    public PerfilRepository() {
        super(Perfil.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
