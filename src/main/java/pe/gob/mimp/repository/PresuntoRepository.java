package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Presunto;

@ApplicationScoped
public class PresuntoRepository extends AbstractRepository<Presunto, Integer> {

    @Inject
    private EntityManager em;

    public PresuntoRepository() {
        super(Presunto.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
