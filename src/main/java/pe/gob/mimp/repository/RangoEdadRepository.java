package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.RangoEdad;

@ApplicationScoped
public class RangoEdadRepository extends AbstractRepository<RangoEdad, Integer> {

    @Inject
    private EntityManager em;

    public RangoEdadRepository() {
        super(RangoEdad.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
