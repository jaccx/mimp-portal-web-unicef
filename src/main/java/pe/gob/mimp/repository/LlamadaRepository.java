package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Llamada;

@ApplicationScoped
public class LlamadaRepository extends AbstractRepository<Llamada, Integer> {

    @Inject
    private EntityManager em;

    public LlamadaRepository() {
        super(Llamada.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
