package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ApplicationScoped
public class JpaProvider {

    @Produces
    @PersistenceContext(name = "unicefPU")
    private EntityManager em;
}
