package pe.gob.mimp.repository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import pe.gob.mimp.entity.Tipologia;

@ApplicationScoped
public class TipologiaRepository extends AbstractRepository<Tipologia, Integer> {

    @Inject
    private EntityManager em;

    public TipologiaRepository() {
        super(Tipologia.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}
