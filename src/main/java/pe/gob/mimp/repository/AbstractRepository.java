package pe.gob.mimp.repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

public abstract class AbstractRepository<T, K> {

    private final Class<T> clazz;

    protected AbstractRepository(Class<T> clazz) {
        this.clazz = clazz;
    }

    protected abstract EntityManager getEntityManager();

    public T findById(K id) {
        EntityManager em = getEntityManager();
        return em.find(clazz, id);

    }

    public T findByName(String name) {
        EntityManager em = getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> bean = cq.from(clazz);
        cq.select(bean)
                .where(
                        cb.equal(bean.get("name"), name)
                );
        List<T> list = em.createQuery(cq)
                .getResultList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Transactional
    public T save(T model) {
        EntityManager em = getEntityManager();
        em.persist(model);
        return model;
    }

    @Transactional
    public List<T> saveAll(List<T> models) {
        EntityManager em = getEntityManager();
        List<T> returns = new ArrayList<>();
        models.forEach(model -> {
            em.persist(model);
            returns.add(model);
        });
        return returns;
    }

    public List<T> findAll() {
        EntityManager em = getEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<T> cq = cb.createQuery(clazz);
        Root<T> bean = cq.from(clazz);
        cq.select(bean);
        return em.createQuery(cq)
                .getResultList();
    }

    @Transactional
    public void deleteById(K id) {
        EntityManager em = getEntityManager();
        T model = findById(id);
        em.remove(model);

    }

}
